package test;

/**
 * Created by Tochi on 2017-04-04.
 */

import java.io.*;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;


public class LemmatizerFile {
    protected StanfordCoreNLP pipeline;

    public static void main(String[] args) {

        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(true);
        int i = 0;

        chooser.setCurrentDirectory(new File("."));

        chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
            public boolean accept(File f) {
                return f.getName().toLowerCase().endsWith(".txt")
                        || f.isDirectory();
            }

            public String getDescription() {
                return "txt";
            }
        });

        int r = chooser.showOpenDialog(new JFrame());
        if (r == JFileChooser.APPROVE_OPTION) {
            File[] curFile = chooser.getSelectedFiles();



            try {
                readAndLemmatizerFile(curFile);
            } catch (IOException e) {

            }


        }
        return;
    }


    public LemmatizerFile() {
        // Create StanfordCoreNLP object properties, with POS tagging
        // (required for lemmatization), and lemmatization
        Properties props;
        props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma");
        this.pipeline = new StanfordCoreNLP(props);
    }



    public List<String> lemmatize(String documentText)
    {
        List<String> lemmas = new LinkedList<String>();
        // Create an empty Annotation just with the given text
        Annotation document = new Annotation(documentText);
        // run all Annotators on this text
        this.pipeline.annotate(document);
        // Iterate over all of the sentences found
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for(CoreMap sentence: sentences) {
            // Iterate over all tokens in a sentence
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
                // Retrieve and add the lemma for each word into the
                // list of lemmas
                lemmas.add(token.get(LemmaAnnotation.class));
            }
        }
        return lemmas;
    }










    public static void readAndLemmatizerFile(File[] files) throws IOException {

        int i = 0;
        List<String> le = new LinkedList<String>();
        while (i < files.length) {
            System.out.println( files[i].getName());
            FileReader fileReader = new FileReader(files[i]);
           //FileWriter fileWriter=new FileWriter(files[i]);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            //BufferedWriter bw = new BufferedWriter(fileWriter);
            String textLine = bufferedReader.readLine();

            do {

                System.out.println(textLine);
                LemmatizerFile slem = new LemmatizerFile();
                System.out.println(slem.lemmatize(textLine));
                le.add(slem.lemmatize(textLine).toString());
                textLine = bufferedReader.readLine();

            } while (textLine != null);

            bufferedReader.close();
            i++;
        }

    }
}
